#Tipos de datos Primitivos en Java

## Unsigned ##

* **char	**	16 bits

Va desde el 0 hasta (2^16)-1

## Signed ##:

* **byte**      8 bits 
* **short**	16 bits
* **int** 	32 bits
* **long**      64 bits

Todos estos tienen van

Desde:  -2^(b-1) 

Hasta: 2^(b-1)-1

Tomando _b_ el numero de bits:

## Números especiales ##

* **float**     32 bits   
* **double**    64 bits

* **boolean**   true   false


## String
Número de caractéres multiplicado por 16 (char)

# Operadores
##Unario / unario:

* ++  suma una unidad
* --  suma dos unidades
* +   le hace positivo
* -   le hace negativo
* !   niega
* ~ convierte 0´s en 1´s y 1´s en 0´s


##Binario

* "+"
* "-"
* "*"
* "/"
* "%"

###Reglas:

La operacion de 2 primitivos:

- resultado sera un dato primitivo numérico
- resultado será por lo menos un enetero
- resultado sera por lo menos igual al dato más grande en bits

La operacion de 2 no primitivos:

- Por lo menos uno de los daos debe ser String
- El resultado será String

###Errores:

-División por 0

## Comparación:

- "<"
- ">"
- "<="
- ">="
- "=="
- "!="

##Bitwise:

- "&"   y
- "^"   o exclusivo
- "|"   o

##Operaciones Binarias:

- "&&" Analiza que solo en un lado sea falso para no pasar
- "||" Analiza que solo en un lado sea verdadero para pasar

- "&" Analiza que los dos sean verdaderos para pasar
- "|" Analiza que los dos sean falsos para no pasar
- "^" Analiza que los dos sean iguales para pasar

#CASTING Y CONVERSIÓN

Explícita e Implícita
##Primitivos
###Conversión
* **Asignación**

Siempre se debe convertir de uno mas pequeño a uno mas grande

double -> float -> long -> int

* **Método**

Pasandole por metodos, se convierte automaticamente

* **Promoción Aritmética**

Cuando estamos sumando dos tipos de datos, siempre el resultado será el tipo de dato mayor

Double + Int = Double

#### NOTAS:

* Cuando a un short le ponemos un carácter unario se transforma a entero

#Casting

- Siempre entre tipos no booleanos
- se pone entre parentesis la variable a la que se quiere pasar

##No Primitivos

###Conversión de referencia

####Asignación

TipoViejo x = new TipoViejo();
TipoNuevo y = x;
* Una interface puede ser oslo convertida a otra interface o a object


* TipoNuevo y TipoViejo deben ser interfaces

* Y TipoViejo debe implementar a TipoNuevo

* Una clase puede ser convertida en una interface
	* Si convetimos de class a interface, el TipoViejo debe implementar a TipoNuevo

* Un array puede convertirse en objeto

####Metodo
Es exactamente igual,
creamos un metodo que nos recibe TipoMascota, y cuando instancio el metodo le mando a llenar por herencia y no va haber inconveniente, el compilador hace la conversion


####Casting de referencia

´´´
TipoViejo tv = new TipoViejo();
TipoNuevo tn = (TipoNuevo)tv;

´´´


- Con 2 clases, una de las clases debe heredar de las otra
- - Siempre se puede hacer cast entre Interface y un objeto "no final"

