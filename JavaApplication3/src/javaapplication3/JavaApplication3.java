/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication3;

/**
 *
 * @author david
 */
public class JavaApplication3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int i = 1000000;
        int x;
        x = i*i;
        System.out.println("i: " + i);
        System.out.println("i^2: " + x);
        System.out.println("No alcanza");
        
        i++;
        System.out.println("i: " + i);
        i--;
        System.out.println("i: " + i);
        i=-i;
        System.out.println("i: " + i);
        i=+i;
        System.out.println("i: " + i);//Sigue siendo negativo  -> menos por mas = menos
        int y=3;
        System.out.println("y: " + y);
        System.out.println("~y: " + ~y);
        //y en binario: 0 011 - Primer digito signo
        //~y en bin:    1 100
        i=-i;
        System.out.println("i+y: " + i+y);
        System.out.println("i-y: " + (i-y));//Da error al poner sin parentesis
        System.out.println("i*y: " + i*y);
        System.out.println("i/y: " + i/y); //La respuesta es en entero
        float div;
        div=i/y;
        System.out.println("Float i/y: " + div); //La respuesta es la misma con un entero pero con un .0  -.-
        double divi;
        divi=i/y;
        System.out.println("double i/y: " + divi); //La respuesta es la misma con un enetro pero con un .0  -.-
        
        int siete = 7;
        int dos = 2;
        float tres = siete/dos;
        System.out.println("División: "+tres);
        
        System.out.println("Ahora casteando cada operador");
        
        float trescast = (float)siete/(float)dos;
        System.out.println("División: "+trescast);
        
        
        System.out.println("Error dividiendo para 0: "+trescast/0);//"Infinity" :O 
        //System.out.println("Error dividiendo para 0: "+dos/0);
        //Un float dividido para 0 da infinity--> no error
        //Un entero si da error
        
        if(dos!=siete|tres!=siete){
            System.out.println("Comprueba los dos lados");
        }
        
        if(dos!=siete|tres!=siete){
            System.out.println("Solo comprueba un lado");
        }
        
        double d = 9.98;
            System.out.println("Float d: "+(float)d);
            System.out.println("Long d: "+(long)d);
            System.out.println("Int d: "+(int)d);
        int sa = 9;
        System.out.println("Suma d + sa: " + (d + sa));
            
        
            
            
        
        
        
        
        
        
        
        
        
    }
    
}
